# hacker-news-scraper

<!---Esses são exemplos. Veja https://shields.io para outras pessoas ou para personalizar este conjunto de escudos. Você pode querer incluir dependências, status do projeto e informações de licença aqui--->

![GitHub repo size](https://img.shields.io/github/repo-size/jmreis/hacker-new-scraper?style=for-the-badge)
![GitHub language count](https://img.shields.io/github/languages/count/jmreis/hacker-new-scraper?style=for-the-badge)
![GitHub forks](https://img.shields.io/github/forks/jmreis/hacker-new-scraper?style=for-the-badge)
![Bitbucket open issues](https://img.shields.io/bitbucket/issues/jmreis/hacker-new-scraper?style=for-the-badge)
![Bitbucket open pull requests](https://img.shields.io/bitbucket/pr-raw/jmreis/hacker-new-scraper?style=for-the-badge)


<img height="300" src="hacker-new-scraper.png" alt="hacker-new-scraper">


> Implementando um script em python com a funcionalidade de buscar novas notícias sobre tecnologia. O site alvo é o https://news.ycombinator.com/news, no script é utilizado o módulo request do python 
para fazer as requisições.


## 💻 Skills

<code>
<img height="50" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/python/python.png" alt="python"/>
<img height="60" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/docker/docker.png" alt="docker"/>
<code>


## 💻 Pré-requisitos

* Docker
* docker-compose


## 🚀 Instalando hacker-new-scraper

Para instalar , siga estas etapas:

```
git clone https://github.com/jmreis/hacker-new-scraper.git

```

## ☕ Usando hacker-new-scraper

Para usar, siga estas etapas:

```
cd hacker-new-scraper

docker-compose up --build
```

 
[⬆ Voltar ao topo](#hacker-new-scraper)<br>

